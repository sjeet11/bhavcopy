from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from pathlib import Path
import time
from zipfile import ZipFile
import redis
import csv

# getting the current folder path
folder = Path().absolute()

# To clear any existing zip files and CSV files
zip_files = [file for file in folder.glob("*.zip")]
for file in zip_files:
    file.unlink()

# To clear any existing CSV files
csv_files = [file for file in folder.glob("*.csv")]
for file in csv_files:
    file.unlink()

# creating Chromeoptions to set donwload folder path
chromeoptions = webdriver.ChromeOptions()

# adding current folder path to preferences
preferences = {"download.default_directory": f"{folder}"}

# adding preferences to prefs
chromeoptions.add_experimental_option("prefs", preferences)


browser = webdriver.Chrome(
    executable_path=r"drivers\chromedriver.exe", chrome_options=chromeoptions)

browser.get("https://www.bseindia.com/markets/MarketInfo/BhavCopy.aspx")

browser.find_element_by_id(
    "ContentPlaceHolder1_btnhylZip").click()

# setting a wait time of 5 seconds to ensure the download starts before closing the browser
time.sleep(5)
browser.close()
browser.quit()

# Getting the zip file in the current folder
for file in folder.glob("*.zip"):
    zipfile = file.name

# Extarting the data from Zipfile  to current folder
with ZipFile(zipfile) as zip:
    zip.extractall()

# Getting the csv file in the current folder
for file_csv in folder.glob("*.csv"):
    csvfile = file_csv.name

# Reading the data from csv file
with open(f"{csvfile}") as file:
    reader = csv.reader(file)
    actual_data = list(reader)
    lenght = len(actual_data)

# connecting to redis database server hosted on virtual machine
redis_data = redis.Redis(host='localhost', port=6379)

# deleting existing data to avoid duplicates
redis_data.delete("code", "name", "open", "high", "low", "close")

# adding data to redis
for x in range(1, lenght):
    redis_data.rpush("code", int(actual_data[x][0]))
    redis_data.rpush("name", actual_data[x][1])
    redis_data.rpush("open", float(actual_data[x][4]))
    redis_data.rpush("high", float(actual_data[x][5]))
    redis_data.rpush("low", float(actual_data[x][6]))
    redis_data.rpush("close", float(actual_data[x][7]))

print("Data updated successfully to redis")
